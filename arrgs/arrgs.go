package arrgs

import (
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"runtime"
	"strings"
	"time"
)

const (
	bakHelp = "Es la `carpeta` en donde se almacenará el contenido de los sitios web."
	blsHelp = "Lista de dominios a bloquear durante la descarga de sitios web."
	domHelp = "Dominio al que deben de pertenecer los sitios web a buscar y descargar."
	urlHelp = "Lista inicial de urls que se utilizarán como punto de partida del proceso."
	htmHelp = "Si esta presente, solo se descargará contenido tipo text/html."
	tskHelp = "Número de `tareas` simúltaneas a utlizar para búsqueda y filtrado de sitios."
	cpuHelp = "Número de `núcleos` disponibles a utilizar."
	cksHelp = "Cantidad de chequeos a realizar en el tiempo de retraso."
	delHelp = "Tiempo de retraso antes de enviar señal de terminación del programa."
)

var (
	// urlChecker es una expresión regular para validar las urls iniciales
	urlChecker *regexp.Regexp
)

var (
	// backup bandera que almacena la ruta en donde se descargarán las
	// páginas web, previo filtrado y deduplicado.
	backup string
	// blackls bandera que almacena una lista de dominios que se desea
	// filtrar.
	blackls string
	// domain bandera que almacena el valor del dominio que se utiliza en el
	// criterio de filtración de sitios por hostname.
	domain string
	// seeds bandera que almacena una lista inicial de urls, a partir de las
	// cuales se realizara el proceso de busqueda, filtrado y descarga de
	// páginas web.
	seeds string
)

var (
	// html bandera de línea de comandos que controla la descarga del contenido
	// de los sitios visitados.
	html bool
)

var (
	// task almacena el numero de tareas simultaneas que se desea utilizar
	task int
	// cpus es el numero de cpus disponibles que se utilizarán.
	cpus int
)

var (
	// checks cantidad total de verificaciones que se realizarán antes de
	// terminar el programa.
	checks int
	// delay tiempo total de espera en el cual se realizarán el número de
	// verificaciones guardados en checks.
	delay time.Duration
)

func init() {
	// Asignación de la expresion regular a utlizar para validar urls
	urlChecker = regexp.MustCompile("https?://[[:alnum:]\\.]?[[:alnum:]\\.]+")

	// Configuración de la ayuda del programa
	flag.Usage = func() {
		fmt.Fprintf(os.Stdout, "uso: %s -urls url_list [-dom domain] [-bkls domain_list] [-path backup_dir]\n", os.Args[0])
		fmt.Fprintf(os.Stdout, "\t[-cpus num_cpus] [-task num_task] [-delay time] [-checks n] [-html]\n\n")
		flag.PrintDefaults()
		fmt.Println()
	}
	// Creación, asignación y validación de las banderas de línea de comandos
	setFlags()
	checkFlags()

	err := os.MkdirAll(backup, os.ModeDir|os.ModePerm)
	if err != nil {
		log.Printf("Error al crear ruta backup: %s\n", err)
	}
	runtime.GOMAXPROCS(cpus)
}

// setFlags crea y asigna valores a las banderas de línea de comandos que
// serán utilizadas en la ejecución del programa.
func setFlags() {
	defDelay := time.Minute * 5
	wd, err := os.Getwd()
	if err != nil {
		wd = os.Getenv("HOME")
	} else {
		wd = strings.TrimPrefix(os.Getenv("HOME"), wd)
	}
	flag.DurationVar(&delay, "delay", defDelay, delHelp)
	flag.StringVar(&backup, "path", wd, bakHelp)
	flag.StringVar(&blackls, "bkls", "", blsHelp)
	flag.StringVar(&domain, "dom", "", domHelp)
	flag.StringVar(&seeds, "urls", "", urlHelp)
	flag.BoolVar(&html, "html", false, htmHelp)
	flag.IntVar(&checks, "checks", 10, cksHelp)
	flag.IntVar(&cpus, "cpus", 1, cpuHelp)
	flag.IntVar(&task, "task", 5, tskHelp)
	flag.Parse()
}

// checkSeedsFlag realiza la validacion de la lista de urls iniciales
func checkSeedsFlag() {
	switch {
	case flag.NFlag() == 1 && seeds == "":
		flag.Usage()
		os.Exit(2)
	case flag.NFlag() == 1 && seeds != "":
		if strings.Contains(seeds, "delay") || strings.Contains(seeds, "path") ||
			strings.Contains(seeds, "bkls") || strings.Contains(seeds, "dom") ||
			strings.Contains(seeds, "html") || strings.Contains(seeds, "checks") ||
			strings.Contains(seeds, "cpus") || strings.Contains(seeds, "task") {
			flag.Usage()
			os.Exit(2)
		}

		urls := strings.Split(seeds, ",")
		for _, u := range urls {
			if ok := urlChecker.MatchString(u); !ok {
				fmt.Println(u)
				fmt.Fprintf(os.Stdout, "\nLista de urls iniciales inválida.\n\n")
				flag.Usage()
				os.Exit(2)
			}
		}
	case flag.NFlag() == 0:
		fmt.Fprintf(os.Stdout, "\nLista de urls iniciales vacía.\n\n")
		flag.Usage()
		os.Exit(2)
	}
}

// checkPathFlag verificación de la ruta a utilizar
func checkPathFlag() {
	if !strings.HasSuffix(backup, "/") {
		backup = backup + "/"
	}
}

// checkCoresFlag validación del numero de procesadores a usar
func checkCoresFlag() {
	switch {
	case cpus <= 0:
		cpus = 1
	case cpus > runtime.NumCPU():
		cpus = runtime.NumCPU()
	}
}

// checkFlags validación de las banderas de línea de comandos
func checkFlags() {
	if !flag.Parsed() {
		flag.Usage()
		os.Exit(2)
	}
	checkSeedsFlag()
	checkPathFlag()
	checkCoresFlag()
}

// GetBakPath devuelve la ruta donde se almacenará el contenido los sitios
// web encontrados.
func GetBakPath() (path string) {
	return backup
}

// GetBlackls devuelve una lista de dominios a restringir en la búsqueda.
func GetBlackls() (blkls []string) {
	return strings.Split(blackls, ",")
}

// GetDomain devuelve el dominio principla al cual deben de pertenecer los
// sitios web encontrados.
func GetDomain() (dom string) {
	return domain
}

// GetSeedList devuelve la lista de urls iniciales, para el proceso de
// búsqueda, flitrado y descarga de sitios web.
func GetSeedList() (urls []string) {
	urls = strings.Split(seeds, ",")
	return
}

// GetOnlyHTML devuelve el valor de la bandera `-html`
func GetOnlyHTML() bool {
	return html
}

// GetNumTask devuelve el número de tareas simultaneas a en la ejecución
func GetNumTask() int {
	return task
}

// GetNumCores devuelve el número de procesadores a utilzar en ejecución
func GetNumCores() int {
	return cpus
}

// GetChecks devuelve el numero de verificaciones a realizar, durante el
// tiempo de espera asignado en `delay`
func GetChecks() int {
	return checks
}

// GetDelay devuelve el tiempo de espera, en el cual se realizaran la
// cantidad de verificaciones asginadas en checks en intervalos de
// igual tiempo, donde los intervalos seran de duración delay/checks.
func GetDelay() time.Duration {
	return delay
}

// BlackListsEmpty devuelve true si la lista de dominios adicionales a
// restringir, esta vacía.
func BlackListsEmpty() (isEmpty bool) {
	bkls := GetBlackls()
	switch {
	case len(bkls) < 1:
		isEmpty = true
	case len(bkls) == 1 && bkls[0] == "":
		isEmpty = true
	default:
		isEmpty = false
	}
	return
}
